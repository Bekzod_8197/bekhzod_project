from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token


from apps.user.views import UserViewSet

router = DefaultRouter()

router.register('data', UserViewSet, 'get_user_data')

urlpatterns = [
    path('', include(router.urls)),
    path('get_token/', obtain_auth_token, name='api_token_auth')
]
