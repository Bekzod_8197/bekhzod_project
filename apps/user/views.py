from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import TemplateView

from rest_framework import viewsets, permissions

from apps.user.serializer import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    Simple CRUD for the user operations
    """
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.IsAuthenticated, ]
